import React,{useState} from 'react';
function Event(){
    const[headingText, setHeadingText]=useState("");
    const[isMousedOver, setMouseOver]=useState(false);
    const[name,setName]=useState("");
    function handleChange(event){
        setName(event.target.value);
    }
    function handleClick(event){
       setHeadingText(name);
       event.preventDefault();
    }
    function handleMouseOver(){

        
        setMouseOver(false);
    }
    function handleMouseOut(){
        setMouseOver(true);
    }
    
    let a =isMousedOver ? "black" : 'white'
    return(
        <div className='Event'>
            <h1>Hello {headingText}</h1>
            <form onSubmit={handleClick}>
            <input
            onChange={handleChange}
            type="text" 
            placeholder="What's your name?" 
            value={name}
            />

            <button
            type="submit"
            style={{backgroundColor: a}}
            onMouseOver={handleMouseOver}
            onMouseOut={handleMouseOut}
            >
                Submit
            </button>
            </form>
            

        </div>
    );
}
export default Event;