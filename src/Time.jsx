import React ,{useState}from "react";

function App2(){
    setInterval(updateTime,1000);
    const now =new Date().toLocaleTimeString();
    const[time , setTime]=useState(now);
    function updateTime(){
        const newTime =new Date().toLocaleTimeString();
        setTime(newTime);
    }
        return(
            <div>
                <h1 style={{color:"green",padding:"5px"}}>{time}</h1>
            </div>
        );
}
export default App2;