import React from "react";
import Input from "./Input.jsx";
function Login(props){
    return(
    <div className="container">
    <Input
    type="text"
    placeholder="Username"
    />
    <Input
    type="password"
    placeholder="Password"
    />
    {props.isRegistered===false && (
    <Input
    type="confirmPassword"
    placeholder="Confirm Password"
    />)}
   <button type="submit">
    {props.isRegistered?"Login" :"Register"}</button>
  </div>
    );
}
export default Login;
